#!/user/bin/python3.9
# Python Script To Download And Install PyCharm Community Edition

import os
import re
import hashlib
import subprocess
from urllib import request
from tqdm import *


# Download Progress Bar (cf. https://tqdm.github.io/docs/tqdm/)
class DownloadProgress(tqdm):
    def show_progress(self, b=1, bsize=1, tsize=None):
        if tsize is not None:
            self.total = tsize
        self.update(b * bsize - self.n)


# Feedback Loop With User (used twice: cf. instances f1 & f2)
class Feedback:
    def __init__(self, input_question, yes_subprocess):
        self.input_question = input_question
        self.yes_subprocess = yes_subprocess

    def feedback_loop(self, yes_feedback="", no_feedback=""):
        feedback = None
        while feedback not in ("y", "n"):
            feedback = str(input(self.input_question).lower().strip())
            if feedback == 'y':
                print(yes_feedback)
                subprocess.call(self.yes_subprocess)
                removed.append(item)  # list of just deleted versions
            elif feedback == 'n':
                print(no_feedback)
                installed.append(item)  # list of also installed versions
            else:
                print('Please enter "y" or "n".')


# Download URL And Show Progress
def download_url(url, output_path):
    with DownloadProgress(unit='B', unit_scale=True, miniters=1, desc=url.split('/')[-1], colour='green') as t:
        request.urlretrieve(url, filename=output_path, reporthook=t.show_progress)


# Get SHA-256 Hash of A File
def get_hash(filename):
    h = hashlib.sha256()  # make a hash object
    with open(filename, 'rb') as file:  # open file for reading in binary mode
        chunk = 0
        while chunk != b'':  # loop till the end of the file
            chunk = file.read(1024)
            h.update(chunk)
    return h.hexdigest()  # return the hex representation


# Read Downloaded Checksum
def read_checksum(file):
    with open(file) as f:
        c = f.read(64)
    return c


# Welcome
print("\nWelcome, this script downloads and installs the PyCharm IDE.")

# Go To /tmp
os.chdir("/tmp")

# Input of PyCharm Version
while True:
    version = input("Please enter the version you want to install (e.g. 2020.3.5): \n").strip()
    if not re.match("^202[0-9].[0-9].?[0-9]?$", version):
        print("Error: Make sure you only use numbers in the correct format.")
    else:
        break

# Download Archive & Checksum
print(f"\nAll right: Downloading PyCharm Community Edition {version} to /tmp...")

archive = "pycharm-community-" + version + ".tar.gz"
checksum = "pycharm-community-" + version + ".tar.gz.sha256"

if os.path.exists(archive):
    print(f"Skipped: The file {archive} already exists.\n")
else:
    try:
        download_url(f"https://download.jetbrains.com/python/{archive}", archive)
        download_url(f"https://download.jetbrains.com/python/{checksum}", checksum)
        print("Complete.\n")
    except Exception as e:
        print(f"Download failed: {e}.\n")

# Print SHA-256 Hash & Checksum
print("Comparing the hash of the archive and the checksum...")
print(get_hash(archive))
print(read_checksum(checksum))

# Verify File's SHA-256 Checksum
if read_checksum(checksum) == get_hash(archive):
    print("The SHA-256 hash of the file matches the checksum.\n")
    print(f"Installation of PyCharm Community Edition {version}...")
    # Check If Version Is Already Installed on System
    if os.path.exists(f"/opt/{archive.split('.tar.gz')[0]}"):
        print("Skipped: This version is already installed on the system.\n")
    else:
        # Unpacking Archive
        print(f"Unpacking {archive} to /opt...")
        subprocess.call(["sudo", "tar", "xfz", archive, "-C", "/opt/"])
        print("Complete.\n")
        # Remove Old Symbolic Link & Create New One To Make PyCharm Easily Accessible To Non-root Users
        print("Note: Created symbolic link in /usr/local/bin to start PyCharm easily.\n")
        subprocess.call(["sudo", "rm", "/usr/local/bin/pycharm+"])
        subprocess.call(
            ["sudo", "ln", "-s", f"/opt/{archive.split('.tar.gz')[0]}/bin/pycharm.sh", "/usr/local/bin/pycharm+"])
else:
    print("Error: The SHA-256 hash of the file does not match the checksum.\n")

# Option To Keep Or Delete Other Versions of PyCharm on The System
print("Looking for other PyCharm versions on the system...")
installed = []
removed = []
for item in os.listdir("/opt"):
    if item.startswith("pycharm-community") and not item.endswith(f"pycharm-community-{version}"):
        # Ask User for Deletion of A Certain Version
        input_question = f"Do you want to delete {item}? [y/n] "
        yes_subprocess = ["sudo", "rm", "-r", f"/opt/{item}"]

        f1 = Feedback(input_question, yes_subprocess)
        f1.feedback_loop()

if len(removed) > 0:
    print(f"Deleted: {', '.join(removed)}")
if len(installed) > 0:
    newline = '\n'
    print(
        f"Installed Versions on The System: \npycharm-community-{version} (just installed) \n{newline.join(installed)}")
else:
    print(f"Note: PyCharm Community Edition {version} is the only installed version on the system.")

# Offer to Start PyCharm
input_question = "\nDo you want to start the last installed version of PyCharm now? [y/n] "
yes_feedback = f"Starting PyCharm Community Edition {version}... \n"
yes_subprocess = "pycharm+"
no_feedback = "Ok, to start PyCharm on the system just type 'pycharm+' in a terminal.\nGood by!\n"

f2 = Feedback(input_question, yes_subprocess)
f2.feedback_loop(yes_feedback, no_feedback)
